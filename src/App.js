// import logo from "./logo.svg";
import "./App.css";
import ExShoeCart from "./ExCartShoes/ExCartShoes";

function App() {
  return (
    <div className="App">
      <ExShoeCart />
    </div>
  );
}

export default App;
