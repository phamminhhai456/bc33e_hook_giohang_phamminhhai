import React from "react";

export default function Cart(props) {
  let renderTbody = () => {
    return props.cart.map((item, index) => {
      return (
        <tr key={index}>
          <td>{item.id}</td>
          <td>{item.name}</td>
          <td>{item.price}</td>
          <td>
            <img src={item.image} alt="" width={100} />
          </td>
          <td>
            <button
              className="btn btn-success"
              onClick={() => props.handleChangeQuantity(item.id, 1)}
            >
              +
            </button>
            <span className="px-2">{item.quantity}</span>
            <button
              className="btn btn-danger"
              onClick={() => props.handleChangeQuantity(item.id, -1)}
            >
              -
            </button>
          </td>
          <td>{item.price * item.quantity}</td>
        </tr>
      );
    });
  };
  return (
    <div>
      <table className="table">
        <thead>
          <tr>
            <th>ID</th>
            <th>Tên</th>
            <th>Giá</th>
            <th>Hình Ảnh</th>
            <th>Số Lượng</th>
            <th>Thành Tiền</th>
          </tr>
        </thead>
        <tbody>{renderTbody()}</tbody>
      </table>
    </div>
  );
}
