import React, { useState } from "react";
import Cart from "./Cart";
import { data_shoes } from "./DataShoe";
import ItemShoe from "./ItemShoe";
export default function ExShoeCart(props) {
  let [state, setState] = useState({
    shoes: data_shoes,
    cart: [],
  });

  let renderContent = () => {
    return state.shoes.map((item, index) => {
      return (
        <ItemShoe key={index} data={item} handleAddToCart={handleAddToCart} />
      );
    });
  };

  let handleAddToCart = (shoe) => {
    let index = state.cart.findIndex((item) => {
      return item.id === shoe.id;
    });
    let cloneCart = [...state.cart];

    if (index === -1) {
      let newShoe = { ...shoe, quantity: 1 };
      cloneCart.push(newShoe);
    } else {
      cloneCart[index].quantity++;
    }
    setState({
      ...state,
      cart: cloneCart,
    });
  };

  let handleChangeQuantity = (id, value) => {
    let index = state.cart.findIndex((item) => {
      return item.id === id;
    });

    let cloneCart = [...state.cart];
    if (index === -1) {
      return;
    }

    cloneCart[index].quantity += value;
    cloneCart[index].quantity === 0 && cloneCart.splice(index, 1);
    setState({
      ...state,
      cart: cloneCart,
    });
  };

  return (
    <div className="container">
      <Cart cart={state.cart} handleChangeQuantity={handleChangeQuantity} />
      <div className="row">{renderContent()}</div>
    </div>
  );
}
