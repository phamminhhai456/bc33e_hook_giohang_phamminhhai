import React from "react";

export default function ItemShoe(props) {
  return (
    <div className="col-3 mt-5">
      <div className="card h-100" style={{ width: "100%" }}>
        <img className="w-100 card-img-top" src={props.data.image} alt="" />
        <div className="card-body">
          <h5 className="card-title">{props.data.name}</h5>
          <button
            onClick={() => {
              props.handleAddToCart(props.data);
            }}
            className="btn btn-secondary"
          >
            Add To Cart
          </button>
        </div>
      </div>
    </div>
  );
}
